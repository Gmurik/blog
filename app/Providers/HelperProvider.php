<?php

namespace App\Providers;

use App\Custom\Classes\Counter;
use App\Custom\Classes\MainMenu;
use Illuminate\Support\ServiceProvider;

class HelperProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //call singleton
        $this->app->singleton('MyCounter', function ($app) {
            return new Counter();
        });

        // call a new instance
        $this->app->bind('MyCounterBind', function ($app) {
            return new Counter();
        });


    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
       // echo 'im helper provider';
    }
}
