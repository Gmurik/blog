<?php

namespace App\Providers;

use App\Menu;
use App\Model\Post;
use App\Model\Tag;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        View::composer('parts.header', function ($view) {
            $menu = $menu = DB::table('menus')->orderBy('weight')->get();
            $view->with(['menu'=> $menu]);
        });

        View::composer(['widgets.categories','widgets.tags'], function ($view){
            $categories = Tag::all();
            $view->with(['categories'=>$categories]);
        });

        View::composer('widgets.posts',function ($view){
            $posts= DB::table('posts')->orderBy('updated_at', 'desc')->take(2)->get();
            $view->with(['posts'=>$posts]);
        });
    }
}
