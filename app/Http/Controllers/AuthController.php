<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegistrRequest;
use App\Model\Post;
use App\Model\Tag;
use App\Model\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class AuthController extends Controller
{

    public function registration()
    {
        return view('layouts.secondary', ['page' => 'pages.signUp', 'title' => 'Registration']);
    }

    public function postRegistration(Request $request, RegistrRequest $registrRequest)
    {
        try {
            $password = Hash::make($request->input('password'));
            $user = User::create([

                'email' => $request->input('email'),
                'password' => $password,
            ]);
            return redirect('/');

        } catch (\Exception $exception) {
            Log::error('error registration', ['error' => $exception->getMessage()]);
        }

    }

    public function login()
    {
        return view('layouts.secondary', ['page' => 'pages.signIn', 'title' => 'Sign In']);
    }


    public function postLogin(Request $request)
    {
        try{
            $remember = $request->input('remember') ? true : false;
            $credentials = $request->only('email', 'password');

            $authResult = (Auth::attempt($credentials, $remember));


            if ($authResult) {
                return redirect()->route('main');
            } else {
                return view('layouts.secondary', ['page' => 'pages.signIn', 'title' => 'Sign In', 'authError' => 'Wrong Login or Password']);
            }
        }catch (\Exception $exception){
          Log::error('error authentication',['error' =>$exception->getMessage()]);
        }



    }

    public function logout()
    {
        try{
            Auth::logout();
            return redirect(route('main'));
        }catch (\Exception $exception){
            Log::error('error  post delete',['error'=>$exception->getMessage(),'post_id']);
        }

    }
}
