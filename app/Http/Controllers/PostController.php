<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePostRequest;
use App\Menu;
use App\Model\Post;
use App\Model\Tag;
use App\Model\User;
use http\Env;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class postController extends Controller
{


    public function postByTag($tag)
    {
        try {
            $postCollection = Tag::where('name', $tag)->first()->posts()->get();
            return view('layouts.primary', ['page' => 'pages.main', 'title' => 'Post by Tag', 'posts' => $postCollection]);
        } catch (\Exception $exception) {
            Log::error('Error display post by tag', ['Error' => $exception->getMessage(), 'tag' => $tag]);
        }


    }


    public function update($id)
    {
        try {
            $this->authorize('update', Post::find($id));
            $post = Post::find($id);
            return view('layouts.secondary', ['page' => 'pages.update_post', 'title' => 'Post', 'post' => $post]);
        } catch (\Exception $exception) {
            Log::error('Error show update post', ['post_id' => $id, 'error' => $exception->getMessage()]);
        }

    }

    public function updatePost(Request $request, $id)

    {
        try {
            $update = Post::find($id);
            $update->title = $request->input('title');
            $update->fulltext = $request->input('fulltext');
            $update->user_id = Auth::id();
            $update->image = $request->input('image');
            $update->save();
            Cache::flush();
            return redirect('/');
        } catch (\Exception $exception) {
            Log::error('Error save updater post', ['error' => $exception->getMessage(), 'post_id' => $id]);
        }


    }

    public function delete($id)
    {
        try {
            $this->authorize('delete', Post::find($id));
            $delete = Post::find($id)->delete();
            Cache::flush();
            return redirect('/');
        } catch (\Exception $exception) {
            Log::error('Error delete post', ['error' => $exception->getMessage(), 'post_id' => $id]);
        }


    }

    public function listPost()
    {
        try {
            $postCollection = cache()->remember('posts', env('CACHE_TIME'), function () {
                return Post::simplePaginate(2);
            });
            return view('layouts.primary', ['page' => 'pages.main', 'title' => 'Post', 'posts' => $postCollection]);
        } catch (\Exception $exception) {
            Log::error('Error display posts', ['error' => $exception->getMessage()]);
        }

    }

    public function onepost($id)
    {
        try {
            $post = Post::find($id);
            return view('layouts.secondary', ['page' => 'pages.onePost', 'title' => 'Post', 'post' => $post]);
        } catch (\Exception $exception) {
            Log::error('error display onepost', ['error' => $exception->getMessage(), 'post_id' => $id]);
        }

    }

    public function showCreatePostForm(Post $post)
    {
        try{
            $this->authorize('create', $post);
            return view('layouts.secondary', ['page' => 'pages.create_new_post', 'title' => 'Create Post']);
        }catch (\Exception $exception){
            Log::error('erreor show create post form',['error'=> $exception->getMessage()]);
        }

    }

    public function createPost(Request $request, CreatePostRequest $createPostRequest)
    {
        try {
            $user = User::find(1)->first();
            $inputTag = $request->input('tag');
            $newtag = Tag::firstOrCreate(['name' => $inputTag]);
            $tagId = Tag::where('name', $inputTag)->first()->id;
            $post = new Post([
                'user_id' => $user->id,
                'image' => $request->input('image'),
                'title' => $request->input('title'),
                'fulltext' => $request->input('fulltext')
            ]);
            $post->save();
            $post->tags()->sync($tagId);
            Cache::flush();
            return redirect('/');
        }catch (\Exception $exception){
            Log::error('error save new post',['error'=>$exception->getMessage()]);
        }

    }
}
