<?php

namespace App\Http\Controllers;

use App\Custom\Classes\Counter;
use App\Http\Requests\BuildMenu;


use App\Http\Requests\FeedbackRequest;
use App\Mail\FeedbackMail;
use App\Model\Menu;
use App\Model\Post;
use App\Model\Profile;
use App\Model\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Redirect;

class others extends Controller
{
    public function showFeedbackForm()
    {
        return view('layouts.primary', ['page' => 'pages.contact_us', 'title' => 'Feedback']);
    }

    public function sendFeedback(Request $request, FeedbackRequest $feedbackRequest)
    {
        try {
            Mail::to('from@example.com')
                ->send(new FeedbackMail($request->all()));
            return view('layouts.primary', ['page' => 'pages.contact_us', 'title' => 'Feedback','status' => 'Your message send successful']);
        }catch (\Exception $exception){
            Log::warning('error send feedback',['error'=>$exception->getMessage(), 'author email'=>$request->email]);
        }

    }

    public function about()
    {
        return view('layouts.primary', ['page' => 'pages.about_us', 'title' => 'About']);
    }


    public function model()
    {
//        create new record in posts table
        $post = new Post([
            'title' => 'new post',
            'content' => 'hfjshdjfhsjdf sdfhshdfjhs dhfjshd jsdhfjshd fdjshfjsd sdfhfjshdf',
            'author' => 'timur'
        ]);
        $post->save();
    }

    public function getModel()
    {
        $post = Post::find('1');
        dump($post);
    }

    public function updateModel()
    {
        $post = Post::find(1);
        $post->author = 'Timur Arslanov';
        $post->save();
    }

//работа с меню
    public function addMenuPost(Request $request, BuildMenu $buildMenu)
    {
        $menu = new Menu([
            'title' => $request->input('title'),
            'link' => $request->input('link'),
            'weight' => $request->input('weight')
        ]);
        $menu->save();

        return redirect('/addmenu');
    }

    public function addMenuForm()
    {

        return view('layouts.secondary', ['page' => 'pages.add_menu', 'title' => 'createmenu']);
    }

//test relationship

//hasone
    public function relation(Post $post)
    {
        $counter = resolve('MyCounter');
$counter->decrement();
dump($counter->getValue());

//       $posts= DB::table('posts')->orderBy('created_at', 'asc')->take(2)->get();
//
//        dump($posts);
//        Mail::raw('Text to e-mail', function($message)
//        {
//            $message->from('from@example.com', 'Laravel');
//            $message->subject('new mail');
//
//            $message->to('rolf_73@mail.ru');
//        });
//       $user_id = $post ->find(1)->user_id;
//        dump($user_id);

//        $user = User::find(1); //update
//        $name = $user->profile->name = 'Timur Arslanov';
//        $user->profile->save();
//        dump($name);


//        $user = Profile::where('name','Timur Arslanov')->first()->user;
//        $user->password = '3333';
//        $user->save();


        //one to many
//        $posts = User::find(1)->posts;
//        foreach ($posts as $post){
//            dump($post->title);
//        }

//        $name = Post::where('title','post')->first()->user->profile->name;
//        dump($name);
//        $tags = Post::where('id', '1')->first()->tags()->get();
//
//        $tagString = '';
//
////        dump($tags);
//        foreach ($tags as $tag){
//            $tagString .= $tag->name;
//        }
//        dump(rtrim($tagString, ','));
    }
}
