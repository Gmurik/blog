<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegistrRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email',
            'password' =>'required|min:8|max:32',
            'passwordConfirm' => 'required|same:password',
            'phone' =>'nullable|regex:/^[+](7)\({1}[0-9]{3}\){1}[0-9]{3}-{1}[0-9]{2}-{1}[0-9]{2}\Z/'
        ];
    }
}
