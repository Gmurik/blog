<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BuildMenu extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=>'required|string',
            'link'=>'required|min:1|max:30',
            'weight'=>'required|numeric'
        ];
    }
}
