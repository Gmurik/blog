<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $guarded = ['id','created_at','updated_at'];

    public function user()
    {
        return $this->belongsTo('App\Model\User');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Model\Tag');
    }
}


