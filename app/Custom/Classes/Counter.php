<?php
/**
 * Created by PhpStorm.
 * User: Владимир
 * Date: 08.08.2019
 * Time: 9:12
 */

namespace App\Custom\Classes;


class Counter
{
    protected $count;


    public function __construct()
    {
        $this->count = 0;
    }


    public function increment()
    {
        return ++$this->count;
    }

    public function decrement(){
        return --$this->count;
    }

    public function getValue(){
        return $this->count;
    }
}