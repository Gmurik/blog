<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->insert([
            'user_id' => '1',
            'title'=>'Новый пост 1',
            'fulltext'=>'содержание статьи содержание статьи содержание статьи содержание статьи содержание статьи содержание статьи содержание статьи ',
            'image'=>'images/image.jpg'
        ]);

    }
}
