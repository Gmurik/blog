<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MenusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('menus')->insert([
            'title' => 'Посты',
            'link' => '/',
            'weight' => '100',
        ]);
        DB::table('menus')->insert([
            'title' => 'регистрация',
            'link' => '/regist',
            'weight' => '101',
        ]);
        DB::table('menus')->insert([
            'title' => 'Добавить меню',
            'link' => '/addmenu',
            'weight' => '103',
        ]);
        DB::table('menus')->insert([
            'title' => 'Новый пост',
            'link' => '/create',
            'weight' => '104',
        ]);
        DB::table('menus')->insert([
            'title' => 'Feedback',
            'link' => '/feedback',
            'weight' => '105',
        ]);
    }
}
