<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

//Route::view('/', 'layouts.primary');


//работа с постами
Route::get('/', 'PostController@listPost')->name('main');
Route::get('create', 'PostController@showCreatePostForm')->middleware('auth');
Route::post('create', 'PostController@createPost');
Route::get('/{id?}', 'PostController@onePost')->where('id', '[0-9]+');
Route::get('update/{id?}','PostController@update')->where('id','[0-9]+')->name('update');
Route::post('update/{id?}','PostController@updatePost')->where('id','[0-9]+')->name('updatePost');
Route::get('delete/{id?}', 'PostController@delete')->where('id','[0-9]+')->name('delete');
Route::get('tag-{tag?}','PostController@postByTag')->name('postByTag')->where('tag','[0-9A-Za-zА-Яа-я]+');

//Route::group(['prefix' => 'post'], function (){
//    Route::get('{id?}', 'PostController@onePost')->where('id', '[0-9]+');
//    Route::get('create', 'PostController@showCreatePostForm');
//    Route::post('create', 'PostController@creatPost');
//    Route::delete('delete/{id}', 'PostController@deletePost')->where('id', '[0-9]+');
//});

//работа с формой обратной связи
Route::get('feedback','others@showFeedbackForm')->name('feedbackForm');
Route::post('feedback','others@sendFeedback');

//работа с About Us
Route::get('about','others@about');

//auth
Route::get('/regist','AuthController@registration');
Route::post('/regist','AuthController@postRegistration');
Route::get('/login','AuthController@login')->name('login');
Route::post('/login','AuthController@postLogin');
Route::get('logout','AuthController@logout')->name('logout');

//test
Route::get('/test','others@model');
Route::get('/gettest','others@getModel');
Route::get('/updatetest','others@updateModel');
Route::get('/relation','others@relation');

//menu generation

Route::get('/addmenu','others@addMenuForm');
Route::post('/addmenu','others@addMenuPost');

