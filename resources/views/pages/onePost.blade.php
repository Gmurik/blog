<div class="boxed  push-down-60">
    <div class="meta">
        <img class="wp-post-image" src="{{$post->image}}" alt="Blog image" width="1138" height="493">
        <div class="row">
            <div class="col-xs-12  col-sm-10  col-sm-offset-1  col-md-8  col-md-offset-2">
                <div class="meta__container--without-image">
                    <div class="row">
                        <div class="col-xs-12  col-sm-8">
                            <div class="meta__info">
                                <span class="meta__date"><span class="glyphicon glyphicon-calendar"></span> &nbsp; 10 мая 2015</span>
                            </div>
                        </div>
                        <div class="col-xs-12  col-sm-4">
                            <div class="comment-icon-counter-detail">
                                <span class="glyphicon glyphicon-comment comment-icon"></span>
                                <span class="comment-counter">10</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-xs-10  col-xs-offset-1  col-md-8  col-md-offset-2  push-down-60">

            <div class="post-content">
                <h1>
                    <a href="#">{{$post->title}}</a>
                </h1>
                <h3>{{$post->tag_line}}</h3>
                <p>
                   {{$post->fulltext}}
                </p>
            </div>

            <div class="row">
                <div class="col-xs-12  col-sm-6">

                    {{--<div class="post-comments">--}}
                        {{--<a class="btn  btn-primary" href="single-post-without-image.html">Комментарии</a>--}}
                    {{--</div>--}}
                    @if(\Illuminate\Support\Facades\Auth::check())
                        <div class="post-comments">
                            <a class="btn  btn-primary" href="update/{{$post->id}}">Edit Post</a>
                            <a class="btn  btn-primary" href="delete/{{$post->id}}">Delete Post</a>
                        </div>


                    @endif
                </div>
            </div>


        </div>
    </div>

</div>