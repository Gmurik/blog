<div class="boxed  push-down-45">
    <div class="row">
        <div class="col-xs-10  col-xs-offset-1">
            <div class="contact">
                <form action="" method="post">
                    <div class="col-md-10" >
                        @csrf

                        @error('title')
                        <div class="error">{{ $message }}</div>
                        @enderror

                        <div class="">
                            Title: <input class="" id="title" type="text" name="title" value="{{$post->title}}">
                        </div>

                        @error('fulltext')
                        <div class="error">{{ $message }}</div>
                        @enderror

                        <div class="">
                            Content: <textarea class="" id="fulltext" name="fulltext" rows="15" cols="100">{{$post->fulltext}}</textarea>
                        </div>

                        {{--@error('tag')--}}
                        {{--<div class="error">--}}
                            {{--{{ $message }}</div>--}}
                        {{--@enderror--}}

                        {{--<div class="">--}}
                            {{--Tag: <input class="" id="tag" type="text" name="tag" value="{{$post->tags->name}}">--}}
                        {{--</div>--}}
                        @error('image')
                        <div class="error">
                            {{ $message }}</div>
                        @enderror

                        <div class="">
                            Image URL: <input class="" id="image" type="text" name="image" value="{{$post->image}}">
                        </div>

                        <div class="col-md-8">
                            <input type="submit" value="Publish">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>