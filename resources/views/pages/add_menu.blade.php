
<div class="boxed  push-down-45">
    <div class="row">
        <div class="col-xs-10  col-xs-offset-1">
            <div class="contact">
                <form action="" method="post">
                    <div class="col-md-6 " >
                        @csrf

                        @error('title')
                        <div class="error">{{ $message }}</div>
                        @enderror

                        <div class="">
                            Title: <input class="" id="title" type="text" name="title" value="{{old('title')}}">
                        </div>

                        @error('link')
                        <div class="error">
                            {{ $message }}</div>
                        @enderror

                        <div class="">
                            Link: <input class="" id="link" type="text" name="link">
                        </div>

                        @error('weight')
                        <div class="error">
                            {{ $message }}</div>
                        @enderror

                        <div class="">
                            Weight: <input class="" id="weight" type="text" name="weight">
                        </div>
                        <div class="col-md-8">
                            <input type="submit" value="Go!">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>