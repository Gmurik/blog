
<div class="boxed  push-down-45">
    <div class="row">
        <div class="col-xs-10  col-xs-offset-1">
            <div class="contact">
                <form action="" method="POST">
                    <div class="col-md-6 " >
                        @csrf

                        @error('email')
                        <div class="error">{{ $message }}</div>
                        @enderror

                        <div class="">
                            Email: <input class="" id="email" type="text" name="email" value="{{old('email')}}">
                        </div>

                        @error('password')
                        <div class="error">
                            {{ $message }}</div>
                        @enderror

                        <div class="">
                            Password: <input class="" id="password" type="password" name="password">
                        </div>
                        <ul>
                           <li> <label for="remember">Remember</label><br>
                            <input style="" class="" id="remember" type="checkbox" name="remember"></li>
                        </ul>
                        <div>{{ $authError ?? ''}}</div>
                        <div class="col-md-8">
                            <input type="submit" value="Go!">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>