<div class="boxed  push-down-45">
    <div class="row">
        <div class="col-xs-10  col-xs-offset-1">
            <div class="contact">
                <form action="" method="post">
                    <div class="col-md-6 " >
                        @csrf
                        {{--@if (isset($errors) && count($errors) > 0)--}}
                        {{--<div class="error" style="color: red; font-size: 18px">--}}
                        {{--<ul>--}}
                        {{--@foreach($errors->all() as $error)--}}
                        {{--<li>{{$error}}</li>--}}
                        {{--@endforeach--}}
                        {{--</ul>--}}
                        {{--</div>--}}
                        {{--@endif--}}

                        @error('email')
                        <div class="error">{{ $message }}</div>
                        @enderror

                        <div class="">
                            Email: <input class="" id="email" type="text" name="email" value="{{old('email')}}">
                        </div>

                        @error('password')
                        <div class="error">
                            {{ $message }}</div>
                        @enderror

                        <div class="">
                            Password: <input class="" id="password" type="password" name="password">
                        </div>
                        @error('passwordConfirm')
                        <div class="error">
                            {{ $message }}</div>
                        @enderror

                        <div class="">
                            Password Confirmation: <input class="" id="passwordConfirm" type="password" name="passwordConfirm">
                        </div>

                        @error('phone')
                        <div class="error">
                            {{ $message }}</div>
                        @enderror

                        <div class="">
                            Phone: <input class="" id="password" type="text" name="phone" placeholder="+7(999)888-22-33">
                        </div>
                        <div class="col-md-8">
                            <input type="submit" value="Registration">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>