<div class="boxed  push-down-45">
    <div class="row">
        <div class="col-xs-10  col-xs-offset-1">
            <div class="contact">
                <h2>Contact Us</h2>
                <p class="contact__text">Please enter your message </p>
                <form action="" method="post">
                    @csrf
                    <div class="row">
                        @error('name')
                        <div class="error">{{ $message }}</div>
                        @enderror
                        <div class="col-xs-6">
                            <input type="text" placeholder="Your Name *" id="name" name="name">
                        </div>
                        @error('email')
                        <div class="error">{{ $message }}</div>
                        @enderror
                        <div class="col-xs-6">
                            <input type="text" placeholder="E-mail Address *" id="email" name="email">
                        </div>
                        @error('message')
                        <div class="error">{{ $message }}</div>
                        @enderror
                        <div class="col-xs-12">
                            <textarea rows="6" type="text" placeholder="Your Message *" id="message" name="message"></textarea>
                            <input type="submit" name="send" value="Send">
                        </div>
                        <p>{{$status ?? ''}}</p>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>