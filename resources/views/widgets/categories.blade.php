<div class="widget-categories  push-down-30">
    <h6>РАЗДЕЛЫ</h6>
    <ul>
        @foreach($categories as $category)
        <li>
            <a href="/tag-{{$category->name}}">{{$category->name}}<span class="widget-categories__text"></span> </a>
        </li>
@endforeach
    </ul>
</div>