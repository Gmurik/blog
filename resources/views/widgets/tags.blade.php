<div class="tags  widget-tags">
    <h6>Тэги</h6>
    <hr>
    @foreach($categories as $category)
        <a href="/tag-{{$category->name}}" class="tags__link">{{$category->name}}</a>
    @endforeach
</div>