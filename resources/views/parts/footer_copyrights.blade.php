<footer class="copyrights">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <div class="copyrights--right">
                    Blog Laravel Template © Copyright <?= date('Y')?>.
                </div>
            </div>
        </div>
    </div>
</footer>