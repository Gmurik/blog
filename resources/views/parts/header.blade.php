<header class="header  push-down-45">
    <div class="container">
        <div class="logo pull-left">
            <a href="/">
                <img src="{{ asset('images/logo.png')}}" alt="Logo" width="352" height="140">
            </a>
        </div>

        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#readable-navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <nav class="navbar navbar-default" role="navigation">
            <div class="collapse  navbar-collapse" id="readable-navbar-collapse">
                <ul class="navigation">
                    @foreach($menu as $item)
                    <li class="dropdown">
                        <a href="{{$item->link}}" class="dropdown-toggle" data-toggle="dropdown">{{$item->title}}</a>
                    </li>
                    @endforeach
                    @if(\Illuminate\Support\Facades\Auth::check())
                        <li class="dropdown"><a href="{{route('logout')}}" class="dropdown-toggle" data-toggle="dropdown">Logout</a></li>
                        @else
                            <li class="dropdown"><a href="{{route('login')}}" class="dropdown-toggle" data-toggle="dropdown">Login</a></li>
                        @endif
                </ul>
            </div>
        </nav>
        <div class="hidden-xs hidden-sm">
            <a href="#" class="search__container  js--toggle-search-mode"> <span class="glyphicon  glyphicon-search"></span> </a>
        </div>
    </div>
</header>